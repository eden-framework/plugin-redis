module gitee.com/eden-framework/plugin-redis

go 1.16

require (
	gitee.com/eden-framework/common v0.0.4
	gitee.com/eden-framework/plugins v0.0.7
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-redis/redis/v8 v8.2.1
	github.com/kr/pretty v0.1.0 // indirect
	github.com/profzone/envconfig v1.4.6
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
